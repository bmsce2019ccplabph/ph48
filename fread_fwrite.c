#include <stdio.h>
#include <string.h>
#include <stdlib.h>
typedef struct student
{
    char name[20];
    float chem,math,physics;
}Student;
Student st;
void file_name(int n,char fileName[n])
{
    printf("Enter file name name.bin\b\b\b\b\b\b\b\b");
    scanf("%s",fileName);
}
FILE* open_file(int n,char fileName[n])
{
    FILE *pt;
    pt=fopen(fileName,"a+");
    if(pt==NULL)
    {
        printf("couldnt open file");
        exit(1);
    }
    return pt;
}
char input1(void)
{
    char in;
    printf("\nDo you want to continue? y/n\n");
    scanf(" %c",&in);
    return in;
}
Student input(void)
{
    Student st;
    printf("\nEnter student name  ");
    scanf("%s",st.name);
    printf("\nmarks in chem ");
    scanf("%f",&st.chem);
    printf("\nmarks in math ");
    scanf("%f",&st.math);
    printf("\nmarks in physics ");
    scanf("%f",&st.physics);
    return st;
}
void searchInput(int n,char name[n])
{
    printf("\nEnter name to search ");
    scanf("%s",name);
}
int length(char str[])
{
    int len=strlen(str);
    return len;
}
Student search(int n,char name[n],char fileName[])
{
    FILE *pt;
    Student st;
    pt=fopen(fileName,"r");
    if(pt==NULL)
    {
        exit(0);
    }
    fseek(pt,0,SEEK_SET);
    while(1)
    {
        fread(&st,sizeof(Student),1,pt);
        if(strcmp(st.name,name)==0)
        {
            printf("\n...\n");
            break;
        }
        else if(feof(pt))
        {
            printf("\nno results found\n");
            break;
        }
    }
    return st;
}
void output(Student st)
{
    printf("Chem marks   :%.3f\nMath marks   :%.3f\nPhysics marks:%.3f",&st.chem,&st.math,&st.physics);
}
int main(void)
{    
    FILE *pt;
    Student st;
    char name[20],fileName[20],in;
    file_name(20,fileName);
    pt=open_file(20,fileName);
    for(;;)
    {
        st=input();
        fwrite(&st,sizeof(Student),1,pt);
        fflush(pt);
        in=input1();
        if(in=='y'||in=='Y')
        {
            continue;
        }
        else
        {
            break;
        }
    }
    fclose(pt);
    searchInput(20,name);
    st=search(20,name,fileName);
    output(st);
    fclose(pt);
}