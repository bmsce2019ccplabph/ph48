//Instructor reviewed commit
#include <stdio.h>
#include <math.h>

struct coordinate
{      float x;
       float y;
}; 
void input(int n,struct coordinate point[n])
{      int i;
       printf("Enter the coordinates of the %d points\n",n);
       for(i=0;i<n;i++)
       {
       scanf("%f,%f",&point[i].x,&point[i].y);
       }
}
void distance(int n,struct coordinate point[n],float dist[n][n])
{      int i,j;
       for(i=0;i<n;i++)
       {   for(j=0;j<n;j++)
           {   
               dist[i][j]=sqrt(pow((point[i].x-point[j].x),2)+pow((point[i].y-point[j].y),2));
           }
       }
}
float small(int n,float dist[n][n])
{      int i,j;
       float min;
       min=dist[0][1];
       for(i=0;i<n;i++)
       {  for(j=0;j<n;j++)
          {   if(j!=i&&dist[i][j]<min)
              {
                 min=dist[i][j];
               }
          }
        }
        return min;
}
void output(int n,float min,float dist[n][n],struct coordinate point[n])
{      
       int i,j;
       for(i=0;i<n;i++)
       {
           for(j=0;j<n;j++)
           {
               if(i!=j)
               printf("\nThe distance between (%.3f,%.3f) and(%.3f,%.3f) is %.3f\n",point[i].x,point[i].y,point[j].x,point[j].y,dist[i][j]);
           }
       }
       if(n>2)
       { 
          printf("\nThe smallest distance is %.3f\n\n",min);
       }
       else
       {  
          printf("\nThe distance is %.3f\n\n",min);
       }
}
int main(void)
{      int n;
       printf("Enter number of points\n");
       scanf("%d",&n);
       float sm_length;
       struct coordinate point[n];
       float dist[n][n];
       if(n>1)
       {
          input(n,point);
          distance(n,point,dist);   
          sm_length=small(n,dist);
          output(n,sm_length,dist,point);
       }
       else 
          { printf("Cannot find distance with %d point\n\n",n);
          }
        return 0;
           
}