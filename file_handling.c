#include <stdio.h>
#include <string.h>
#include <stdlib.h>
typedef struct student
{
    char name[20];
    float chem,math,physics;
}Student;
Student st;
void file_name(int n,char fileName[n])
{
    printf("Enter file name name.txt\b\b\b\b\b\b\b\b");
    scanf("%s",fileName);
}
FILE* open_file(int n,char fileName[n])
{
    FILE *pt;
    pt=fopen(fileName,"a+");
    if(pt==NULL)
    {
        printf("couldnt open file");
        exit(1);
    }
    return pt;
}
char input1(void)
{
    char in[2];
    printf("\nDo you want to continue? y/n\n");
    scanf("%c%c",&in[0],&in[1]);
    return in[1];
}
Student input(FILE *pt)
{
    Student st;
    printf("\nEnter student name  ");
    scanf("%s",st.name);
    printf("\nmarks in chem ");
    scanf("%f",&st.chem);
    printf("\nmarks in math ");
    scanf("%f",&st.math);
    printf("\nmarks in physics ");
    scanf("%f",&st.physics);
    return st;
}
void searchInput(int n,char name[n])
{
    printf("\nEnter name to search ");
    scanf("%s",name);
}
int length(char str[])
{
    int len=strlen(str);
    return len;
}
FILE* search(int n,char name[n],char fileName[])
{
    FILE *pt;
    pt=fopen(fileName,"r");
    if(pt==NULL)
    {
        exit(0);
    }
    fseek(pt,0,SEEK_SET);
    char str[20];
    while(1)
    {
        fscanf(pt,"%s",str);
        if(strcmp(str,name)==0)
        {
            printf("\n...\n");
            break;
        }
        else if(feof(pt))
        {
            printf("\nno results found\n");
            break;
        }
       /* else 
        {
            len=length(str);
            fseek(pt,(len-1),SEEK_CUR);
        }*/
    }
    return pt;
}
void output(FILE *pt)
{
    if(feof(pt))
    {
        return ;
    }
    char str[20];
    fscanf(pt,"%s",str);
    printf("Chem    %s\n",str);
    fscanf(pt,"%s",str);
    printf("Math    %s\n",str);
    fscanf(pt,"%s",str);
    printf("Physics %s\n",str);
}
int main(void)
{    
    FILE *pt;
    Student st;
    char name[20],fileName[20],in;
    file_name(20,fileName);
    pt=open_file(20,fileName);
    for(;;)
    {
        st=input(pt);
        fprintf(pt,"%s %.03f %.03f %.03f\n",st.name,st.chem,st.math,st.physics);
        fflush(pt);
        in=input1();
        if(in=='y'||in=='Y')
        {
            continue;
        }
        else
        {
            break;
        }
    }
    fclose(pt);
    searchInput(20,name);
    pt=search(20,name,fileName);
    output(pt);
    fclose(pt);
}