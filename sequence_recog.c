#include <stdio.h>
int input(void)
{
    int in=0,num=0;
    printf("Enter the numbers with a -1 to end\n");
    while(1)
    {
        scanf("%d",&in);
        if(in==-1)
        {
            break;
        }
        num=num*10+in;
    }
    return num;
}
int compute(int in)
{
    int digit,temp;
    temp=in;
    while(temp!=0)
    {
        in=temp;
        digit=in%10;
        while(in!=0)
        {
            in/=10;
            if(digit==(in%10))
            {
                return 0;
            }
            else
            {
                continue;
            }
        }
        temp/=10;
    }
    return 1;
}
void output(int res)
{
    printf("\n%d",res);
}
int main(void)
{
    int in,result;
    in=input();
    result=compute(in);
    output(result);
}