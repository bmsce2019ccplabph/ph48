//final commit
#include <stdio.h>
struct student_marks
{
    char name[128];
    float physics,chemistry,maths;
};
typedef struct student_marks student;
int count()
{
    int n;
    printf("Enter number of students ");
    scanf("%d",&n);
    return n;
}
void input(int n,student st[n])
{
    int i;
    for(i=0;i<n;i++)
    {
        printf("\n\nEnter\nStudent name ");
        scanf("%s",&st[i].name);
        printf("Physics marks ");
        scanf("%f",&st[i].physics);
        printf("Chemistry marks ");
        scanf("%f",&st[i].chemistry);
        printf("Math marks ");
        scanf("%f",&st[i].maths);
    }
}
void sort(int n,student st[n])
{
    student temp;
    int i,j,k;
    for(i=0;i<n;i++)
    {
        for(j=0;j<(n-1);j++)
        {
            if(st[j].physics>st[(j+1)].physics)
            {
                temp=st[j];
                st[j]=st[(j+1)];
                st[(j+1)]=temp;
            }
            else if(st[j].physics==st[(j+1)].physics)
            {
                if(st[j].chemistry>st[(j+1)].chemistry)
                {
                    temp=st[j];
                    st[j]=st[(j+1)];
                    st[(j+1)]=temp;
                }
                else if(st[j].chemistry==st[(j+1)].chemistry)
                {
                    if(st[j].maths>st[(j+1)].maths)
                    {
                        temp=st[j];
                        st[j]=st[(j+1)];
                        st[(j+1)]=temp;
                    }
                    else 
                    {
                        for(k=0;k<(n-1);k++)
                        {
                            if(st[j].name[k]>st[(j+1)].name[k])
                            {
                                temp=st[j];
                                st[j]=st[(j+1)];
                                st[(j+1)]=temp;
                                break;
                            }
                            else continue;
                        }
                    }
                }
            }
        }
    }
}
void output(int n,student st[n])
{
    int i;
    for(i=0;i<n;i++)
    {
        printf("\nStudent name: %s   physics marks: %.3f  chemistry marks: %.3f  math marks: %.3f",st[i].name,st[i].physics,st[i].chemistry,st[i].maths);
    }
}
int main(void)
{
    int n;
    n=count();
    student st[n];
    input(n,st);
    sort(n,st);
    output(n,st);
    printf("\n\n");
    return 3;
}