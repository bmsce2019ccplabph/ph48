#include <stdio.h>
int count(void)
{
    int count;
    printf("Enter the number of figures to add ");
    scanf("%d",&count);
    return count;
}
void input(int n,float in[n])
{
      int i;
      printf("Enter 1st number  ");
      scanf("%f",&in[0]);
      printf("Enter 2nd number  ");
      scanf("%f",&in[1]);
      printf("Enter 3rd number  ");
      scanf("%f",&in[2]);
      for(i=3;i<n;i++)
      {
          printf("Enter %dth number  ",(i+1));
          scanf("%f",&in[i]);
      }
}
void add(int n,float in[n],float *sum)
{
    int i;
    *sum=0;
    for(i=0;i<n;i++)
    {
        *sum+=in[i];
    }
}
void output(float sum)
{
    printf("\nThe sum is %.3f\n\n",sum);
}
int main(void)
{
    int n;
    n=count();
    float in[n],sum;
    input(n,in);
    add(n,in,&sum);
    output(sum);
    return 'a';
}