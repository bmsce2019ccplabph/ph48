//initial commit
#include <stdio.h>
int input()
{  
    int n;
    printf("Enter a number to find multiplication tables\n");
    scanf("%d",&n);
    return n;
}
void compute(int n,int m,int table[m])
{
    int i;
    for(i=0;i<m;i++)
    {   
        table[i]=(i+1)*n;
    }
}
void output(int n,int m,int table[m])
{  
    int i;
    for(i=0;i<m;i++)
    {  
        printf("\n%d*%d=%d",n,(i+1),table[i]);
    } 
}
int main(void)
{   
    int n,table[10];
    n=input();
    compute(n,10,table);
    output(n,10,table);
    return 0;
}