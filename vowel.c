//Final commit
#include <stdio.h>
void input(int n,char in[n])
{
    int i;
    char enter;
    printf("\nEnter %d characters\n",n);
    for(i=0;i<n;i++)
    {
        scanf("%c",&enter);
        scanf("%c",&in[i]);
    }
}
int number()
{
    int n;
    printf("Enter the number of characters ");
    scanf("%d",&n);
    return n;
}
void compute(int n,char in[n],int res[n])
{
    int i;
    for(i=0;i<n;i++)
    {
       if(in[i]=='a'||in[i]=='A'||in[i]=='e'||in[i]=='E'||in[i]=='u')
       {
          res[i]=1;  
       } 
       else if(in[i]=='i'||in[i]=='I'||in[i]=='o'||in[i]=='O'||in[i]=='U')
       {
           res[i]=1;
       }
       else res[i]=0;
    }
}
void output(int n,int res[n],char in[n])
{
    int i,count;
    count=0;
    for(i=0;i<n;i++)
    {
        if(res[i]==1)
        {
            printf(" %c is a vowel\n",in[i]);
            count++;
        }
        else printf(" %c is a consonant\n",in[i]);
    }
    printf("You entered %d vowels and %d consonants",count,(n-count));
}
int main(void)
{
    int n;
    n=number();
    if(n>0)
    {
        char in[n];
        int res[n];
        input(n,in);
        compute(n,in,res);
        output(n,res,in);
    } 
    else printf("Then why else do you use this program");
    return 0;
}