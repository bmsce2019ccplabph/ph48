//Using structures
#include <stdio.h>
#include <math.h>
struct coordinate
{
    float x;
    float y;
};
typedef struct coordinate point;
point input()
{
    point p;
    printf("Enter abcissa ");
    scanf("%f",&p.x);
    printf("Enter ordinate ");
    scanf("%f",&p.y);
    return p;
}
float compute(point p1,point p2)
{
    float distance;
    distance=sqrt(pow((p1.x-p2.x),2)+pow((p1.y-p2.y),2));
    return distance;
}
void output(float dist)
{
    printf("The distance is %.3f",dist);
}
int main(void)
{
    float dist;
    point p1,p2;
    p1=input();
    p2=input();
    dist=compute(p1,p2);
    output(dist);
    return 0;
}