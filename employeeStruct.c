#include <stdio.h>
#include <string.h> 
typedef struct date_of_joining
{
    int day,month,year;
}JoinDate;
typedef struct employee_details
{
    char name[20];
	int age;
	long int id;
    JoinDate doj;
	float salary;
}Employee_profile;
int number(void)
{
    int n;
	printf("Enter number of employees ");
	scanf("%d",&n);
	return n;
}
void input(int n,Employee_profile employee[n])
{
    int i;
	for(i=0;i<n;i++)
	{
	   printf("Enter:\nName:");
	   scanf("%s",employee[i].name);
	   printf("Id number:");
	   scanf("%ld",&employee[i].id);
	   printf("Date of joining:dd/mm/yyyy\b\b\b\b\b\b\b\b\b\b");
	   scanf("%d/%d/%d",&employee[i].doj.day,&employee[i].doj.month,&employee[i].doj.year);
	   printf("Age:");
	   scanf("%d",&employee[i].age);
	   printf("Current Salary:");
	   scanf("%f",&employee[i].salary);
	}
}
void search_input(int n,char srchName[n])
{
    scanf("%s",srchName);
}
int search(int m,int n,char srchName[m],Employee_profile employee[n])
{
    int i;
	for(i=0;i<n;i++)
	{
	    if(strcmp(srchName,employee[i].name)==0)
		{
		   return i;
		}
	}
	return (n+1);
}
void output(Employee_profile employee)
{
    printf("\nEnter:\nName                :%s",employee.name);
    printf("\nId number          :%ld",employee.id);
    printf("\nDate of Joining    :%02d/%02d/%02d",employee.doj.day,employee.doj.month,employee.doj.year);
    printf("\nAge                :%d",employee.age);
    printf("\nCurrent Salary     :%f",employee.salary);
}
int main(void)
{
    int n,i;
	n=number();
	char srchName[20];
    Employee_profile employee[n];
	input(n,employee);
	printf("\n\nEnter employee name to search or * to quit ");
	search_input(20,srchName);
	do
	{
	   i=search(20,n,srchName,employee);
       if(i<n)
	   {
           output(employee[i]);
       }
       else
       {
           printf("\nEmployee not found!!!!");
       }
	   printf("\n\nEnter employee name to search or * to quit ");
	   search_input(20,srchName);
	}while(srchName[0]!='*');
	return 0;
}