//Edited commit using array of structures
#include <stdio.h>

struct fraction
{      int num;
       int den;
};

void input(int count,struct fraction fract[count])
{      int i;
       printf("\nEnter first fraction\n");
       for(i=0;i<(count-1);i++)
       {   scanf("%d/%d",&fract[i].num,&fract[i].den);
           printf("\nEnter next fraction\n");
       }
       scanf("%d/%d",&fract[(count-1)].num,&fract[(count-1)].den);
}

struct fraction compute(int count,struct fraction fract[count])
{      int i,j,term;
       struct fraction sum; 
       sum.num=0;
       sum.den=1;
       for(i=0;i<count;i++)
       {   term=fract[i].num;
           for(j=0;j<count;j++)
           {   if(j==i)
                  {continue;}
               else if(j!=i)
               {term*=fract[j].den;}
            }
           sum.num+=term;
           sum.den*=fract[i].den;
       }
       return sum;
}

struct fraction simplify(struct fraction sum)
{      int i,gcd;
       gcd=1;
       for(i=2;i<=sum.num&&i<=sum.den;i++)
       {   if(sum.num%i==0&&sum.den%i==0)
            {
                gcd=i;
            }
           else 
            {
               continue;
            }
       }
       sum.num=sum.num/gcd;
       sum.den=sum.den/gcd;
       return sum;
}

void output(struct fraction sum)
{      printf("The sum is %d/%d",sum.num,sum.den);
}

int main()
{      int count;
       struct fraction sum,simple;
       printf("Enter number of fractions: \n");
       scanf("%d",&count);
       struct fraction fract[count];
       input(count,fract);
       sum=compute(count,fract);
       simple=simplify(sum);
       output(simple);
       return 0;
}