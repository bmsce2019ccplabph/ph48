#include <stdio.h>
int input(void)
{
    int in;
    printf("Enter the value of n or enter 0 to quit ");
    scanf("%d",&in);
    return in;
}
int compute(int in)
{
    int count=0,f=0;
    while(f!=1)
    {
       if(in%2==0)
       {
          in/=2;
       }
       else 
       {
          in=in*3+1;
       }
       f=in;
       count++;
    }
    return count;
}
void output(int count)
{
    printf("%d\n\n",count);
}
int main(void)
{
    int in,count;
    in=input();
    while(in!=0)
    {
        count=compute(in);
        output(count);
        in=input();
    }
    return 0;
}