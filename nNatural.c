#include <stdio.h>
int input(void)
{
    int in;
    printf("Enter the value of n  ");
    scanf("%d",&in);
    return in;
}
void compute(int n, int array[n])
{
    for(int i=0;i<n;i++)
    {
        array[i]=n-i;
    }
}
void output(int n,int array[n])
{
    for(int i=0;i<n;i++)
    {
        printf("%d ",array[i]);
    }
}
int main(void)
{
    int in=input();
    int array[in];
    compute(in,array);
    output(in,array);
    return 0;
}