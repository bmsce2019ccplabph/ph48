#include <stdio.h>
#include <string.h>
void output(int length,char string[length])
{
    printf("\nThe new string is %s\n",string);
}
int string_length(int n,char str[n])
{
    int i,length=0;
	for(i=0;i<n;i++)
	{
	     if(str[i]!='\0')
		 {
		      length++;
         }
	}
    return length;
}
void concatenate(int n,char string1[n],char string2[n])
{
    int length,len1;
	len1=string_length(n,string1);
	length=string_length(n,string2);
    length+=len1;
	char string[length];
    sprintf(string,"%s%s",string1,string2);
    output(length,string);
}
int main(void)
{
    char string1[20],string2[20];
	printf("Enter first string ");
    gets(string1);
    printf("Enter second string ");
    gets(string2);
	concatenate(20,string1,string2);
	return 0;
}