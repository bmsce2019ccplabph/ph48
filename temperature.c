#include <stdio.h>

float input(void)
{   float far;
    printf("Enter temperature in fareinheit");
    scanf("%f",&far);
    return far;
}
float compute(float far)
{   float cel;
    cel=(far-32)*(5.0/9);
    return cel;
}
void output(float cel)
{   printf("The temperature in celcius is %f",cel);
}
int main(void)
{   float cel,far;
    far=input();
    cel=compute(far);
    output(cel);
    return 0;
}