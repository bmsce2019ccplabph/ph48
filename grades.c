//Edited commit after compilation
#include <stdio.h>
#include <string.h>
struct subject_score
{
    float marks[5];
    char name[5][10];
};
typedef struct subject_score sub;
void input1(int n,sub subject[n])
{
    int i,j,k;
    printf("Enter the subjects\n");
    for(i=0;i<5;i++)
    {
        scanf("%s",&subject[0].name[i]);
    }
    for(i=1;i<n;i++)
    {
        for(j=0;j<5;j++)
        {
            for(k=0;k<10;k++)
            {
                subject[i].name[j][k]=subject[0].name[j][k];
                
            }
        }
    }
}
void input(int n,sub subject[n],char st_name[n][10])
{
    int i,j;
    printf("Enter:\n");
    for(i=0;i<n;i++)
    {
        printf("Student name\n");
        scanf("%s",st_name[i]);
        for(j=0;j<5;j++)
        {
            printf("%s score: ",subject[i].name[j]);
            scanf("%f",&subject[i].marks[j]);
        }
    }
}
char compute(int n,sub subject[n],float total[n],float percent[n],char grade[n])
{
    int i,j;
    for(i=0;i<n;i++)
    {
        total[i]=0;
        percent[i]=0;
        for(j=0;j<5;j++)
        {
            total[i]+=subject[i].marks[j];
        }
        percent[i]=total[i]/5;
        if(percent[i]>90)
        {
            grade[i]='S';
        }
        else if(percent[i]>80)
        {
            grade[i]='A';
        }
        else if(percent[i]>70)
        {
            grade[i]='B';
        }
        else if(percent[i]>60)
        {
            grade[i]='C';
        }
        else if(percent[i]>50)
        {
            grade[i]='D';
        }
        else if(percent[i]>40)
        {
            grade[i]='E';
        }
        else grade[i]='F';
    }
}
void output(int n,sub subject[n],char st_name[n][10],float total[n],float percent[n],char grade[n])
{
    int i,j;
    for(i=0;i<n;i++)
    {
        printf("\nName : %s",st_name[i]);
        for(j=0;j<5;j++)
        {
            printf("\n%s : %f",subject[i].name[j],subject[i].marks[j]);
        }
        printf("\nTotal : %f",total[i]);
        printf("\nPercent : %f",percent[i]);
        printf("\nGrade : %c",grade[i]);
    }
}
int main(void)
{
    int n;
    printf("Enter the number of students");
    scanf("%d",&n);
    float total[n],percent[n];
    char  grade[n];
    sub subject[n];
    char st_name[n][10];
    input1(n,subject);
    input(n,subject,st_name);
    compute(n,subject,total,percent,grade);
    output(n,subject,st_name,total,percent,grade);
    return 0;
}