#include <stdio.h>
#include <math.h>
typedef struct coordinate
{
    float x,y;
}Coordinate;
int inputLoop()
{
    int loop;
    printf("Enter the number of lines\n");
    scanf("%d",&loop);
    return loop;
}
Coordinate input()
{
    Coordinate P;
    scanf("%f,%f",&P.x,&P.y);
    return P;
}
float SLope(Coordinate P,Coordinate Q)
{
    float slope;
    slope=(P.y-Q.y)/(P.x-Q.y);
    return slope;
}
Coordinate midpoint(Coordinate P,Coordinate Q)
{
    Coordinate MP;
    MP.x=(P.x+Q.x)/2.0;
    MP.y=(P.y+Q.y)/2.0;
    return MP;
}
float lineLength(Coordinate P,Coordinate Q)
{
    float length;
    length=sqrt(pow((P.x-Q.x),2)+pow((P.y-Q.y),2));
    return length;
}
void calculate(Coordinate P,Coordinate Q,Coordinate *P_,Coordinate *Q_)
{
    float slope,length;
    Coordinate MP;
    slope=SLope(P,Q);
    length=lineLength(P,Q);
    MP=midpoint(P,Q);
    length/=2;
    P_->x=MP.x+(length*slope/sqrt(1+slope*slope));
    P_->y=MP.y-(length/sqrt(1+slope*slope));
    Q_->x=MP.x-(length*slope/sqrt(1+slope*slope));
    Q_->y=MP.y+(length/(sqrt(1+slope*slope)));
}
void output(Coordinate P,Coordinate Q,Coordinate P_,Coordinate Q_)
{
    printf("\nRotating (%.2f,%.2f)(%.2f,%.2f)  yields (%.2f,%.2f)(%.2f,%.2f)\n",P.x,P.y,Q.x,Q.y,P_.x,P_.y,Q_.x,Q_.y);
}
int main(void)
{
    Coordinate P,Q,P_,Q_;
    int loop,i;
    loop=inputLoop();
    for(i=1;i<=loop;i++)
    {
        printf("\nEnter the coordinates of the end points of the line\n");
        P=input();
        Q=input();
        calculate(P,Q,&P_,&Q_);
        output(P,Q,P_,Q_);
    }
}