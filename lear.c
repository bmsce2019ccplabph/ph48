//Final commit
#include <stdio.h>
void input(int *year)
{
    printf("Enter a year\n");
    scanf("%d",year);
}
void compute(int year,int *res)
{
    if(year%100==0&&year%400==0)
    {
        *res=1;
    }
    else if(year%100!=0&&year%4==0)
    {
        *res=1;
    }
    else *res=0;
}
void output(int year,int res)
{
    if(res==1)
    {
        printf("%d is a leap year\n\n",year);
    }
    else printf("%d is not a leap year\n\n",year);
}
int main(void)
{
    int year,res;
    input(&year);
    compute(year,&res);
    output(year,res);
    return 0;
}