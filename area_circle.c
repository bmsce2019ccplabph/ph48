//initial commit
#include <stdio.h>
#include <math.h>
#define pi 22.0/7
float input(void)
{   
    float radius;
    printf("Enter the radius of the circle\n");
    scanf("%f",&radius);
    return radius;
}
float compute(float radius)
{   
    float area;
    area=pi*(pow(radius,2));
    return area;
}
void output(float area)
{   printf("The area is %.3f",area);
}
int main(void)
{   
    float radius,area;
    radius=input();
    area=compute(radius);
    output(area);
    return 0;
}