#include <stdio.h>
int inputSize(void)
{
    int in;
    scanf("%d",&in);
    return in;
}
void input(int size, int array[size])
{
    for(int i=0;i<size;i++)
    {
        scanf("%d",&array[i]);
    }
}
void merge(int size1,int array1[size1],int size2,int array2[size2],int array[size1+size2])
{
    int i=0,j=0,k=0;//i corresonds to array1, j to array2 and k to array3
    while((i<size1)&&(j<size2))
    {
         if(array1[i]<array2[j])
         {
            array[k]=array1[i];
            i++;
            ++k;
         }
         else if(array1[i]>array2[j])
         {
            array[k]=array2[j];
            j++;
            ++k;
         }
         else
         {
            array[k]=array2[j];
            j++;
            k++;
            array[k]=array1[i];
            i++;
            ++k;
         }
    }
    if(i==size1)
    {
        while(j<size2)
        {
            array[k]=array2[j];
            j++;
            ++k;
        }
    }
    else if(j==size2)
    {
        while(i<size1)
        {
            array[k]=array1[i];
            i++;
            ++k;
        }
    }
}
void output(int size, int array[size])
{
    for(int i=0;i<size;i++)
    {
        printf("%d ",array[i]);
    }
}
int main(void)
{
    int size1,size2;
    printf("Enter size of first array  ");
    size1=inputSize();
    printf("\nEnter size of second array  ");
    size2=inputSize();
    int array1[size1],array2[size2],array[size1+size2];
    printf("\nEnter elements of array1\n");
    input(size1,array1);
    printf("\nEnter elements of array2\n");
    input(size2,array2);
    merge(size1,array1,size2,array2,array);
    output((size1+size2),array);
    return 0;
}