#include <stdio.h>
#include <string.h>
int main(void)
{
    FILE *f;
    char c,filename[50];
    printf("Enter file name\nname.txt\b\b\b\b\b\b\b\b");
    scanf("%s",filename);
    printf("File successfully opened!!!\nEnter contents of file");
    f=fopen(filename,"w");
    while((c=getchar())!=EOF)
    {
        putc(c,f);
    }
    fclose(f);
    printf("\nContents of %s are\n",filename);
    f=fopen(filename,"r");
    while((c=getc(f))!=EOF)
    {
        putchar(c);
    }
    fclose(f);
    return 0;
}