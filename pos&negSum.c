#include<stdio.h>
float input(void)
{
    float in;
    scanf("%f",&in);
    return in;
}
void compute(float *posSum,float *negSum,int *posCount,int *negCount,float in)
{
    if(in<-1)
    {
        *negSum+=in;
        ++*negCount;
    }
    else if(in>-1)
    {
        *posSum+=in;
        ++*posCount;
    }
}
void output(float posSum,float negSum,int posCount,int negCount)
{
    printf("\n\nPositive sum=%03.3f\tPositive mean=%03.3f\n",posSum,(posSum/posCount));
    printf("Negative sum=%03.3f\tNegative mean=%03.3f\n\n",negSum,(negSum/negCount));
}
int main(void)
{
    int posCount=0,negCount=0;
    float in,posSum=0.0,negSum=0.0;
    printf("Enter numbers with -1 to quit");
    in=input();
    while(in!=-1)
    {
         compute(&posSum,&negSum,&posCount,&negCount,in);
         in=input();
    }
    output(posSum,negSum,posCount,negCount);
    return 0;
}