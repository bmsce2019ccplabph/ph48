#include<stdio.h>
#include<string.h>
#include<math.h>
struct sentence
{
	int n;
	char str[100];
};

int count()
{
    int no;
    scanf("%d",&no);
    return no;
}
/*   the input of the string id taken here and stored into the structure sentence creates..    
*/
void input(int size, struct sentence in[size])
{
    int inp;
    for(int i=0;i<size;i++)
    {
        scanf("%d\n%[^\n]",&inp,in->str[i]); 
        in->n[i]=inp;
    }
}

void display(int index, int n, char substring[n])
{
    printf("\n%d %s",(index+1),substring);
}

void check(int index,int n, char substring[n])
{
    int i=0,j=n-2;
    while(i<=j)
    {
        if(substring[i]==substring[j])
        {
            i++;
            j--;
        }
        else 
        {
            return;
        }
    }
    display(index,n,substring);
}
/*   here the string is thoroughly checked for palindromes present in the inputed string...    
*/
void check_sub_palindromes(struct sentence a)
{
	int j,i,l;
	for(l=0;l<a.n;l++)
	{	
		i=3;
		while(i<a.n)
		{
		    j=0;
		    char substring[i+1];
		    while(j<=i)
		    {
		        substring[j]=a.str[(l+j)];
		        j++;
		    }
		    substring[i]='\0';
		    i++;
		    check(l,i,substring);
		}
	}	
}

void compute(int size, struct sentence in[size])
{
    for(int i=0;i<size;i++)
    {
        check_sub_palindromes(in[i]);
    }
}

int main()
{
    int size=count();
	struct sentence in[size];
	input(size,in);
	compute(size,in);
	return 0;
}