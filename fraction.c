//Initial commit
#include <stdio.h>
struct fraction
{
    int num,den;
};
void input(struct fraction *f1,struct fraction *f2)
{
    f1->den=1;
    f2->den=1;
    printf("Enter the first fraction ");
    scanf("%d/%d",&f1->num,&f1->den);
    printf("\nEnter the second fraction ");
    scanf("%d/%d",&f2->num,&f2->den);
}
void add(struct fraction *f1,struct fraction *f2,struct fraction *sum)
{
    sum->num=f1->num*f2->den+f2->num*f1->den;
    sum->den=f1->den*f2->den;
}
void simplify(struct fraction *sum)
{
    int i,gcd;
    gcd=1;
    for(i=2;i<=sum->num&&i<=sum->den;i++)
    {
        if(sum->num%i==0&&sum->den%i==0)
         {
           gcd=i;
         }
        else continue;
    }
    sum->num=sum->num/gcd;
    sum->den=sum->den/gcd;
}
void output(struct fraction *f1,struct fraction *f2,struct fraction *sum)
{
    printf("\nThe sum of %d/%d and %d/%d is %d/%d\n\n",f1->num,f1->den,f2->num,f2->den,sum->num,sum->den);
}
int main(void)
{
    struct fraction f1,f2,sum;
    input(&f1,&f2);
    if(f1.den&&f2.den)
    {
        add(&f1,&f2,&sum);
        simplify(&sum);
        output(&f1,&f2,&sum);
    }
    else
    {
        printf("\nThe sum is infinity\n\n");
    }
    return 0;
}