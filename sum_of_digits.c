//initial commit
#include <stdio.h>

int input(void)
{   int num;
    printf("Enter number\n");
    scanf("%d",&num);
    return num;
}
int compute(int num)
{   int sum;
    sum=0;
    while(num!=0)
    {    sum+=(num%10);
         num/=10;
        }
    return sum;
}
void output(int sum)
{   printf("The sum of its digits are %d",sum);
}
int main(void)
{   int num,sum;
    num=input();
    sum=compute(num);
    output(sum);
    return 0;
}