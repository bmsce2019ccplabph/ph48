//initial commit
#include <stdio.h>
void input(int n,float number[n])
{   
    int i;
    printf("Enter the first number\n");
    for(i=0;i<(n-1);i++)
    { 
        scanf("%f",&number[i]);
        printf("Enter the next number\n");
    }
    scanf("%f",&number[n-1]);
}
float compute(int n,float number[n])
{
    int i;
    float sum;
    sum=0;
    for(i=0;i<n;i++)
    {
        sum+=number[i];
    }
    return sum;
}
void output(float sum)
{
    printf("The sum of numbers is %f",sum);
}
int main(void)
{
    int count;
    float sum;
    printf("Enter the count of numbers to be added\n");
    scanf("%d",&count);
    float number[count];
    if(count>1)
    {
       input(count,number);
       sum=compute(count,number);
       output(sum);
    }
    else printf("You need minimum of 2 numbers to add\n");
    return 0;
}