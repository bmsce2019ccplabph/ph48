#include <stdio.h>
int inputSize(void)
{
    int in;
    printf("Enter the number of elements  ");
    scanf("%d",&in);
    return in;
}
void inputArray(int n,int array[n])
{
    printf("\nEnter elements");
    for(int i=0;i<n;i++)
    {
        scanf("%d",&array[i]);
    }
}
void delete_small(int m,int small[m],int n,int array[n])
{
    for(int i=0;i<n;i++)
    {
        if(array[i]==small[0])
        {
            array[i]=small[1];
        }
    }
}
int Check_similar(int check,int n,int array[n])
{
    int count=0;
    for(int i=0;i<n;i++)
    {
        if(array[i]==check)
        {
            count++;
        }
    }
    return count;
}
int Small(int n,int array[n])
{
    int small[2]={array[0],array[0]};
    for(int i=1;i<n;i++)
    {
        if(small[0]>array[i])
        {
            small[0]=array[i];
        }
        if(small[1]<array[i])
        {
            small[1]=array[i];
        }
    }
    for(int i=1;i<n;i++)
    {
        if(array[i]==small[0])
        {
            continue;
        }
        else if(array[i]<small[1])
        {
            small[1]=array[i];
        }
    }
    delete_small(2,small,n,array);
    return small[0];
}
int checkArray2(int small,int size,int array[size])
{
    for(int i=0;i<size;i++)
    {
        if(array[i]==small)
        {
            return 1;
        }
    }
    return 0;
}
int compute(int size1,int size2,int array1[size1],int array2[size2])
{
    int check=1,small;
    while(check)
    {
        small=Small(size1,array1);
        if(small<0)
        {
            break;
        }
        check=checkArray2(small,size2,array2);
    }
    return small;
}
void output(int small)
{
    if(small>=0)
    {
        printf("\nSmallest uncommon element is %d \n\n",small);
    }
    else 
    {
        printf("\No uncommon element exists\n\n");
    }
}
int main(void)
{
    int size1,size2,small;
    printf("Array 1:\n");
    size1=inputSize();
    int array1[size1];
    inputArray(size1,array1);
    printf("Array 2\n");
    size2=inputSize();
    int array2[size2];
    inputArray(size2,array2);
    small=compute(size1,size2,array1,array2);
    output(small);
    return 0;
}