#include <stdio.h>

struct student_profile
{      char name[20];
       int age;
       float height;
       char sec;
};

void input(int count,struct student_profile st[count])
{      int i;
       for(i=0;i<count;i++)
       {   printf("\nStudent Name   : ");
           scanf("%s",&st[i].name);
           printf("\n        Age    : ");
           scanf("%d",&st[i].age);
           printf("\n        Height : ");
           scanf("%f",&st[i].height);
           printf("\n        Section: ");
           scanf(" %c",&st[i].sec);
       }
}

int compute(void)
{      return 0;
}

void output(int count,struct student_profile st[count])
{      int i;
       for(i=0;i<count;i++)
       {   printf("\n%dStudent Name   :%s",i,st[i].name);
           printf("\n          Age    :%d",st[i].age);
           printf("\n          Height :%f",st[i].height);
           printf("\n          Section:%c",st[i].sec);
       }
}

int main(void)
{      int count;
       printf("Enter number of students:");
       scanf("%d",&count);
       struct student_profile st[count];
       input(count,st);
       compute();
       output(count,st);
       printf("\n");
       return 0;
}