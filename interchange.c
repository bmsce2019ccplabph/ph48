#include <stdio.h>
int number(void)
{
    int n;
	printf("Enter the number of elements\n");
	scanf("%d",&n);
	return n;
}
void input(int n,float in[n])
{
    int i;
	printf("\nEnter the numbers\n");
	for(i=0;i<n;i++)
	{
	    scanf("%f",&in[i]);
	}
}
void swap(int n,float in[n])
{
    int i,lar,sm;
	lar=0;
	sm=0;
	for(i=1;i<n;i++)
	{
	    if(in[i]>in[lar])
		{
		   lar=i;
		}
		else if(in[i]<in[sm])
		{
		   sm=i;
		}
	}
	in[lar]=in[lar]+in[sm];
	in[sm]=in[lar]-in[sm];
	in[lar]=in[lar]-in[sm];
}
void output(int n,float in[n])
{
    int i;
	printf("\n The elements after swap are:\n");
	for(i=0;i<n;i++)
	{
	    printf("%.3f\n",in[i]);
	}
}
int main(void)
{
    int n;
	n=number();
	float in[n];
	input(n,in);
	swap(n,in);
	output(n,in);
	return 0;
}