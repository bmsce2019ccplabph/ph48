#include <stdio.h>
typedef enum primComp
{
    NP_C=1,PRIME,COMPOSITE
}PrimComp;
int input(void)
{
    int in;
    printf("\nEnter a number(or 0 to quit)\n");
    scanf("%d",&in);
    return in;
}
int factors(int in)
{
    int count=0,i;
    for(i=1;i<=in;i++)
    {
        if(in%i==0)
        {
            count++;
        }
    }
    return count;
}
PrimComp Check(int in)
{
    int count;
    PrimComp p_c=1;
    count=factors(in);
    switch(count)
    {
        case 1:
            p_c=NP_C;
            break;
        case 2:
            p_c=PRIME;
            break;
        default:
            p_c=COMPOSITE;
            break;
    }
    return p_c;
}
void output(int in,PrimComp p_c)
{
    switch(p_c)
    {
        case NP_C:
             printf("%d is neither prime nor composite\n\n",in);
             break;
        case PRIME:
             printf("%d is prime\n\n",in);
             break;
        case COMPOSITE:
             printf("%d is composite\n\n",in);
             break;
        default:
             break;
    }
}
int main(void)
{
    int in;
    PrimComp p_c;
    while(1)
    {
        in=input();
        if(in==0)
        {
            break;
        }
        p_c=Check(in);
        output(in,p_c);
    }
    return 0;
}